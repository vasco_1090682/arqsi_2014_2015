<?
require_once ("apis_keys.php");
//rawurlencode para poder enviar parametros com caracteres especiais
$tag=rawurlencode($_GET['tag']); //tag selecionada
$ntracks=rawurlencode($_GET['ntracks']); //número de tracks a mostrar

// pedido ao last.fm com a função file_gets_contents
// a string XML devolvida pelo servidor last.fm fica armazenada na variável $respostaXML
$respostaXML=file_get_contents("http://ws.audioscrobbler.com/2.0/?method=tag.gettoptracks&tag=".$tag."&limit=".$ntracks."&api_key=".$lastfmAPI);

// criar um objecto DOMDocument e inicializá-lo com a string XML recebida
$newXML= new DOMDocument('1.0', 'ISO-8859-1');
$newXML->loadXML($respostaXML);

// navegar no XML com os métodos que já conhece, mas com uma sintaxe PHP para
// aceder a objectos(->)
$nodelist=$newXML->getElementsByTagName("track");

//construir xml
$xml = '<?xml version="1.0" encoding="UTF-8"?>';
$xml .= "\n<tracklinksinfo>\n";
for ($i=0;$i<$nodelist->length;$i++)
{
  $xml .= "<info>\n";
  
  //guardar o mbid da track para uso futuro
  $mbidTrack=trim($nodelist->item($i)->childNodes->item(5)->nodeValue);
  
  //nome da track
  $trackNameNode=$nodelist->item($i)->childNodes->item(1); //primeiro filho do elemento track
  $trackName = $trackNameNode->nodeValue;
  $xml .= "<trackname>".trim(xml_entities($trackName))."</trackname>\n";
  
  //album da track
  $album =file_get_contents("http://phpdev2.dei.isep.ipp.pt/~i090682/trabalho/php/albumOfTrack?mbid=".rawurlencode($mbidTrack));
  $albumInfo = explode(",",$album);
  $albumName = trim($albumInfo[0]);
  $mbidAlbum = trim($albumInfo[1]);
  $xml .= "<album>".trim(xml_entities($albumName))."</album>\n";
  
  //capa do album da track
  $albumCover =file_get_contents("http://phpdev2.dei.isep.ipp.pt/~i090682/trabalho/php/coverOfAlbum?mbid=".rawurlencode($mbidAlbum));
  $xml .= "<albumcover>".trim(xml_entities($albumCover))."</albumcover>\n";
  
  //nome do artista
  $artistNameNode=$nodelist->item($i)->childNodes->item(11)->childNodes->item(1); //primeiro filho do elemento track
  $artistName = $artistNameNode->nodeValue;
  $xml .= "<artistname>".trim(xml_entities($artistName))."</artistname>\n";
  
  //imagem do artista, size 11 devolve tamanho large
  //7 = small, 9 = medium, 11 = large, 13 = extralarge, 15 = mega
  $size = 15;
  $artistImg = file_get_contents("http://phpdev2.dei.isep.ipp.pt/~i090682/trabalho/php/imageOfArtist?artista=".rawurlencode($artistName)."&size=".$size);
  $xml .= "<artistimg>".trim(xml_entities($artistImg))."</artistimg>\n";
  
  //top 3 albuns do artista
  $albumsResponse = file_get_contents("http://phpdev2.dei.isep.ipp.pt/~i090682/trabalho/php/topAlbumsByArtist?artista=".rawurlencode($artistName)."&nalbums=3");
  $top3albums=explode(",",$albumsResponse);
  for ($j=0;$j<3;$j++)
  {
	if($top3albums[$j]!=NULL)
	{
		$xml .= "<top3album>".trim(xml_entities($top3albums[$j]))."</top3album>\n";
	}
  }
  
  //1ª top track do artista
  $topTrackArtista = file_get_contents("http://phpdev2.dei.isep.ipp.pt/~i090682/trabalho/php/topTracksByArtist?artista=".rawurlencode($artistName)."&ntracks=1");
  $xml .= "<toptrack>".trim(xml_entities($topTrackArtista))."</toptrack>\n";
  
  $xml .= "</info>\n";
}
$xml .= "</tracklinksinfo>";

Header('Content-type: text/xml');
echo $xml;

//métodos auxiliares
//substituir caracteres especiais
function xml_entities($string) {
    return str_replace(
        array("&",     "<",    ">",    '"',      "'"),
        array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"), 
        $string
    );
}

//registar pedido
include './pedidos/registarPedido.php';
echo getUrlPedido("/php/topTracksByTag?tag=".$tag."&ntracks=".$ntracks);
?> 

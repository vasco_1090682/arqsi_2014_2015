<?
require_once ("apis_keys.php");
//rawurlencode para poder enviar parametros com caracteres especiais
$artista=rawurlencode($_GET['artista']); //nome do artista
$nalbums=rawurlencode($_GET['nalbums']); //

// pedido ao last.fm com a função file_gets_contents
// a string XML devolvida pelo servidor last.fm fica armazenada na variável $respostaXML
$respostaXML=file_get_contents("http://ws.audioscrobbler.com/2.0/?method=artist.gettopalbums&artist=".$artista."&limit=".$nalbums."&api_key=".$lastfmAPI);

// criar um objecto DOMDocument e inicializá-lo com a string XML recebida
$newXML= new DOMDocument('1.0', 'ISO-8859-1');
$newXML->loadXML($respostaXML);

// navegar no XML com os métodos que já conhece, mas com uma sintaxe PHP para
// aceder a objectos(->)
$nodelist=$newXML->getElementsByTagName("album");
$albums="";
for ($i=0;$i<$nodelist->length;$i++)
{
  $albumNameNode=$nodelist->item($i)->childNodes->item(1); //nome do album
  $albumName = $albumNameNode->nodeValue;
  
  //não colocar vírgula após o último album
  if ($i+1 == $nodelist->length)
    $albums.=$albumName;
  else
    $albums.=$albumName . ",";
}

//imprimir informação
echo $albums;

//registar pedido
include './pedidos/registarPedido.php';
echo getUrlPedido("/topAlbumsByArtist?artista=".$artista."&nalbums=".$nalbums);
?> 

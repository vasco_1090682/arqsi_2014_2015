var xmlHttpObj=null;
var request=null; //variável que vai indicar no stateHandler que código deve correr
var numPags=null; //número de páginas de eventos
var pagAtual=null; //página atual dos eventos
function CreateXmlHttpRequestObject( )
{ 
  if (window.XMLHttpRequest)
  {
      xmlHttpObj=new XMLHttpRequest();
  }
  else if (window.ActiveXObject)
  {
    xmlHttpObj=new ActiveXObject("Microsoft.XMLHTTP");
  }
  return xmlHttpObj;
}

function getArtistTags()
{     
  xmlHttpObj = CreateXmlHttpRequestObject();

  //artista introduzido na text box
  var artistaElem=document.getElementById("tartista");
  var artista=artistaElem.value;

  // efectuar pedido Ajax ( open, registar eventHandler, send , etc)
  if (xmlHttpObj)
  {
    // Definição do URL para efectuar pedido HTTP - método GET
    xmlHttpObj.open("GET","php/topTagsByArtist.php?artista="+artista, true);

    // Registo do EventHandler
    request="getArtistTags";
    xmlHttpObj.onreadystatechange = stateHandler;
    xmlHttpObj.send(null);
  }
}

function getTopTracksByTag()
{     
  xmlHttpObj = CreateXmlHttpRequestObject();

  //tag seleciona no select
  var selectTags=document.getElementById("stags");
  var index = selectTags.selectedIndex;
  var tagSelecionada = selectTags.options[index].text;
  
  //número de tracks a mostrar
  var ntracksElem=document.getElementById("tntracks");
  var ntracks=ntracksElem.value;
  
  // efectuar pedido Ajax ( open, registar eventHandler, send , etc)
  if (xmlHttpObj)
  {
    // Definição do URL para efectuar pedido HTTP - método GET
    xmlHttpObj.open("GET","php/topTracksByTag.php?tag="+tagSelecionada+"&ntracks="+ntracks, true);

    // Registo do EventHandler
    request="getTopTracksByTag";
    xmlHttpObj.onreadystatechange = stateHandler;
    xmlHttpObj.send(null);
  }
}

function getEvents()
{
	xmlHttpObj = CreateXmlHttpRequestObject();
	if (xmlHttpObj)
	{
		//vai buscar a localização indicada
		var locElem=document.getElementById("tlocal");
		var loc=locElem.value;
		
		//vai buscar a distância indicada
		var distElem=document.getElementById("tdist");
		var dist=distElem.value;
	
		// Definição do URL para efectuar pedido HTTP - método GET
		xmlHttpObj.open("GET","php/getEvents.php?location="+loc+"&distance="+dist, true);

		// Registo do EventHandler
		request="getEvents";
		xmlHttpObj.onreadystatechange = stateHandler;
		xmlHttpObj.send(null);
	}
}

function stateHandler()
{ 
  if ( xmlHttpObj.readyState == 4 && xmlHttpObj.status == 200)
  { 
    if (request == "getArtistTags")
    {
      // recebe resposta
      var respostaText = xmlHttpObj.responseText;
      var vecTags = respostaText.split(",");
      preencheSelectTags(vecTags);
    } else if (request=="getTopTracksByTag")
    {
      var docxml = xmlHttpObj.responseXML;
      var nodeList = docxml.getElementsByTagName("info");
      construirTabelaTopTracks(nodeList);
    } else if (request=="getEvents")
	{
		var docxml = xmlHttpObj.responseXML;
		var nodeList = docxml.getElementsByTagName("event");
		var coordenadasElem = docxml.getElementsByTagName("coordenadas");
		var coordenadas = coordenadasElem[0].childNodes[0].nodeValue;
		
		//validar se nodelist.length é maior que zero
		if (nodeList.length > 0)
		{
			construirPaginacaoEMapa(nodeList,coordenadas);
		} else {
			//mostra uma mensagem a indicar que não encontrou eventos
			noEventsMessage();
		}
	}
  }
}

function construirPaginacaoEMapa(nodeList, coordenadas)
{
	var numEvents = nodeList.length; //número de eventos
	var eventsPorPagina = 5; //número máximo de eventos a aprensentar por página
	pagAtual = 1;
	//calcular número de páginas necessárias
	var modEvents = Math.floor(numEvents/5); //divisão inteira do nº eventos por nº eventos por página
	var divisaoEvents = numEvents/5;
	if  (divisaoEvents > modEvents)
	{
		numPags = modEvents+1;
	} else numPags = divisaoEvents;
	
	rodapeEvents(pagAtual,numPags);
	construirPaginas(nodeList, eventsPorPagina);
	construirMapa(nodeList, coordenadas);
	//metodo aqui para fazer o mapa, enviando a nodeList
}

//constroi os divs que têm cada página
function construirPaginas(nodeList, eventsPorPagina)
{
	var cont = 0; //contador para contar o nº de elementos que vão aparecendo numa lista
	var str = "";
	
	//vai buscar a localização indicada
	var locElem=document.getElementById("tlocal");
	var local=locElem.value;
	
	for (var i = 0; i<nodeList.length; i++)
	{
		if(cont == 0) //se o conta for 0, significa que estamos no primeiro evento de uma página
		{
			cont++;
			if (i == 0) //mas se o i for 0, é o primeiro evento de todos, e o div deve ser uma class diferente dos outros
			{
				str += "<div class='divEventsInitial' id='divEvents"+i+"'>";
				str += "<table class='tableEvents' vocab='http://schema.org/' resource='#"+local+"'typeof='events'>";
				str += "<tr class='tableRowEvents' typeof='Event'>";
			} else { //se o i for outro número qualquer para além de 0 quando o cont é 0, significa que também estamos no principio de uma nova página mas não a primeira de todas
				str += "<div class='divEventsOthers' id='divEvents"+i+"'>";
				str += "<table class='tableEvents'>";
				str += "<tr class='tableRowEvents' typeof='Event'>";
			}
		} else { //se o cont for diferente de 0, que vamos adicionar um row a um div e tabela existentes
			cont++;
			str += "<tr class='tableRowEvents'>";		
		}
		
		//preencher as row
		//nome do evento
		var noEventName= nodeList[i].getElementsByTagName("name")[0];
		var eventName = noEventName.childNodes[0].nodeValue;
		str += "<td class='tableTdNameEvents' property='name'>"+eventName+"</td>";
		
		//data do evento
		var noEventDate= nodeList[i].getElementsByTagName("date")[0];
		var eventDate = noEventDate.childNodes[0].nodeValue;
		str += "<td class='tableTdDateEvents' property='doorDate'>"+eventDate+"</td>";
		
		//fechar row
		str += "</tr>";
		
		if (cont == (eventsPorPagina)) //se for o último event da página, reinicializza o contador, e fecha a table e o div
		{
			str += "</table>";
			str += "</div>";
			cont=0;
		}
	}
	document.getElementById("divPagEvents").innerHTML = str;
	document.getElementById("divPagEvents").style.display = 'block';
	document.getElementById("divPagEvents").style.visibility = 'visible';
}

//rodepé com os botões das páginas
function rodapeEvents(pagAtual,numPags)
{
	var rodape = "<input id ='btTras' value='<' type='button' onclick='tratamentoPaginacao(\"btTras\")'>";
	rodape += " "+pagAtual+" de "+numPags+" ";
	rodape += "<input id ='btFrente' value='>' type='button' onclick='tratamentoPaginacao(\"btFrente\")'>";
	document.getElementById("pPages").innerHTML = rodape;
	mostrarElem("pPages");
}

function noEventsMessage()
{
	//vai buscar a localização indicada
	var locElem=document.getElementById("tlocal");
	var loc=locElem.value;

	//vai buscar a distância indicada
	var distElem=document.getElementById("tdist");
	var dist=distElem.value;

	//limpar o div da tabela de eventos
	document.getElementById("divPagEvents").innerHTML = "";

	//e indicar uma mensagem ao utilizador
	var msg = "Não foram encontrados eventos para "+loc+" num raio de "+dist+" Km.";
	document.getElementById("pPages").innerHTML = msg;
	mostrarElem("pPages");
	
	//e esconde o div com a tabela de eventos caso lá tivesse informação de outra pesquisa
	document.getElementById("divPagEvents").style.display = 'none';
	document.getElementById("divPagEvents").style.visibility = 'hidden';
}


//muda para a página seguinte ou anterior
function tratamentoPaginacao(bt)
{
	if((pagAtual >= 1 && pagAtual < numPags) && bt == "btFrente")
	{
		var pagAberta = document.getElementById("divEvents"+((pagAtual-1)*5));
		var pagSeguinte = document.getElementById("divEvents"+(pagAtual*5));
		mudarPagina(pagAberta,pagSeguinte);
		pagAtual++;
	} else if ((pagAtual <= numPags && pagAtual >1) && bt == "btTras") {
		var pagAberta = document.getElementById("divEvents"+((pagAtual-1)*5));
		var pagSeguinte = document.getElementById("divEvents"+((pagAtual-2)*5));
		mudarPagina(pagAberta,pagSeguinte);
		pagAtual--;
	}
	rodapeEvents(pagAtual,numPags);
}

function mudarPagina(pag1, pag2)
{
	//esconde a página aberta
	pag1.style.display = 'none';
	pag1.style.visibility = 'hidden';
	//e mostra seguinte
	pag2.style.display = 'block';
	pag2.style.visibility = 'visible';
}


//preenche o select das tags de um determinado artista
function preencheSelectTags(vecTags)
{
  var selectTags=document.getElementById("stags");
  // apagar options anteriores
  if (selectTags.length>0)
  {
    while (selectTags.firstChild) // remove todos os filhos do nó
    {
      selectTags.removeChild(selectTags.firstChild);
    }
  }

  // carregar select tags usando Dom
  var optionEscolher = document.createElement("option");
  optionEscolher.setAttribute("value",0);
  selectTags.appendChild(optionEscolher);
  var optionTextEscolher = document.createTextNode("Escolher...");
  optionEscolher.appendChild(optionTextEscolher);
  
  var i;
  for (i=0;i<vecTags.length;i++)
  {
    var option = document.createElement("option");
    option.setAttribute("value",i+1);
    selectTags.appendChild(option);
    var optionText = document.createTextNode(vecTags[i]);
    option.appendChild(optionText);
  }
}

function construirTabelaTopTracks(nodeList)
{
	//tag seleciona no select
	var selectTags=document.getElementById("stags");
	var index = selectTags.selectedIndex;
	var tagSelecionada = selectTags.options[index].text;

	var str = "<table class='tableTracks' vocab='http://schema.org/' resource='#"+tagSelecionada+"' typeof='tracks'>";
	for (i=0;i<nodeList.length;i++)
	{
		var noTrackName= nodeList[i].getElementsByTagName("trackname")[0];
		var trackname = noTrackName.childNodes[0].nodeValue;
		str += "<tr class='tableRowTracks' typeof='track'><td>"+(i+1)+"</td><td><a class='linkTableTrakcs' property='name' href='javascript:void(0)' onclick=\"toggleToolTip("+i+","+nodeList.length+");\">" + trackname + "</a>" + construirToolTip(nodeList, i) + "</td></tr>";
	}
	str += "</table>";

	// alteração do conteúdo textual do div
	document.getElementById("divTracks").innerHTML = str;
}

function construirToolTip(nodeList, i)
{	
	var str = "<div class='popup' id=divtt"+i+" vocab='http://schema.org/'><table><tr>";
	
	//informação sobre a track numa célula
	str += "<td>";
	
	//nome do album da track
	str += "<p class='titulosToolTip'>Album</p>";
	var noAlbumTitle= nodeList[i].getElementsByTagName("album")[0];
	if (noAlbumTitle.childNodes[0] != null)
	{
		var albumTitle = noAlbumTitle.childNodes[0].nodeValue;
		str += "<p typeof='MusicAlbum'>"+albumTitle+"</p>";
	} else {
		str += "<p>N/A</p>";
	}
	
	//capa do album da track
	var noAlbumCover= nodeList[i].getElementsByTagName("albumcover")[0];
	if (noAlbumCover.childNodes[0] != null)
	{
		var albumCover = noAlbumCover.childNodes[0].nodeValue;
		str += "<p><img class='imgToolTip' src='"+ albumCover +"'></p>";
	} else {
		str += "<p><img class='imgToolTip' src='img/noalbum.png'></p>";
	}
	
	//fechar célula
	str += "</td>";
	
	//informação sobre o artista numa célula
	str += "<td>";
	
	//nome do artista
	str += "<p class='titulosToolTip'>Artista</p>";
	var noArtistName= nodeList[i].getElementsByTagName("artistname")[0];
	if (noArtistName.childNodes[0] != null)
	{
		var artistName = noArtistName.childNodes[0].nodeValue;
		str += "<p>"+artistName+"</p>";
	} else {
		str += "<p>N/A</p>";
	}
	
	
	//imagem do artista
	var noArtistImage= nodeList[i].getElementsByTagName("artistimg")[0];
	if (noArtistImage.childNodes[0] != null)
	{
		var artistImage = noArtistImage.childNodes[0].nodeValue;
		str += "<p><img class='imgToolTip' src='"+ artistImage +"'></p>";
	} else {
		str += "<p><img class='imgToolTip' src='img/noartist.jpg'></p>";
	}
	//top track do artista
	str += "<p class='titulosToolTip'>Top Track</p>";
	var noArtistTopTrack= nodeList[i].getElementsByTagName("toptrack")[0];
	if (noArtistTopTrack.childNodes[0] != null)
	{
		var artistTopTrack = noArtistTopTrack.childNodes[0].nodeValue;
		str += "<p>"+artistTopTrack+"</p>";
	} else {
		str += "<p>N/A</p>";
	}
	
	//top 3 albuns do artista
	str += "<p class='titulosToolTip'>Top 3 Albuns</p>";
	var noArtistTop3Albums = nodeList[i].getElementsByTagName("top3album");
	if (noArtistTop3Albums[0].childNodes[0] != null)
	{
		var strAux = "";
		for (var j=0;j<noArtistTop3Albums.length;j++)
		{
			var artistTop3Albums = noArtistTop3Albums[j].childNodes[0].nodeValue;
			strAux += artistTop3Albums+"</br>"
		}
		str += "<p>"+strAux+"</p>";
	} else {
		str += "<p>N/A</p>";
	}
	
	str += "</tr></table></div>";
	return str;
}

//abrir/fechar tooltip
function toggleToolTip(id,tam) 
{
	var elemId = "divtt"+id;
	var e = document.getElementById(elemId);
	if(e.style.visibility == 'visible') 
	{
		e.style.display = 'none';
		e.style.visibility = 'hidden';
	} else {
		e.style.display = 'block';
		e.style.visibility = 'visible';
		//quando o divtt pretendido está a hidden e quer-se passá-lo para visible deve-se hidden todos os outros
		for (var i = 0; i<tam;i++)
		{
			if (id != i)
			{
				var e_aux = document.getElementById("divtt"+i);
				e_aux.style.display = 'none';
				e_aux.style.visibility = 'hidden';
			}
		}
	}
}

//coloca um gif a ilustar o loading
function loadingImg(id)
{
	//colocar uma imagem de loading enquanto não recebe os dados
	var loadingImg = "<img src='img/loading.gif' width='150' height='auto'>";
	document.getElementById(id).innerHTML = loadingImg;
}

function loadingImgEvents(id)
{
	//colocar uma imagem de loading enquanto não recebe os dados
	var loadingImg = "<img src='img/loading.gif' width='150' height='auto'>";
	document.getElementById(id).innerHTML = loadingImg;
	
	//exibir o div com a gif
	mostrarElem("divPagEvents");
	
	//esconder o div rodape e mapa caso estejam abertos
	esconderElem("pPages");
	esconderElem("map-canvas");
}

function mostrarElem(id)
{
	document.getElementById(id).style.display = 'block';
	document.getElementById(id).style.visibility = 'visible';
}

function esconderElem(id)
{
	document.getElementById(id).innerHTML = "";
	document.getElementById(id).style.display = 'none';
	document.getElementById(id).style.visibility = 'hidden';
}

//limpar o conteúdo do divTracks quando se clica em Pesquisar por tags
function limparDivTracks()
{
	document.getElementById("divTracks").innerHTML = "";
}

function construirMapa(nodeList, coordenadas) {
	mostrarElem("map-canvas");
	//center mapa na localização indicada
	var coord_aux = coordenadas.split(",");
	var latCenter = coord_aux[0];
	var lngCenter = coord_aux[1];
	
	//criar object coordenadas com as coordenadas da localização indicada pelo utilizador
	var myLatlng = new google.maps.LatLng(latCenter, lngCenter);
	var mapOptions = {
		zoom: 12,
		center: myLatlng
	}
	
	var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	
	//marker da localização indicada
	//vai buscar a localização indicada
	var locElem=document.getElementById("tlocal");
	var loc=locElem.value;
	var marker = new google.maps.Marker({
		position: myLatlng,
		map: map,
		title: loc,
		icon: 'img/marker.png'
	});
	
	//opções do circulo que cobre a área um dado raio a partir da localização indicada
	//distância indicada
	var distElem=document.getElementById("tdist");
	var dist=distElem.value;
	
	var circleOptions = {
      strokeColor: '#FF0000',
      strokeOpacity: 0.5,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.25,
      map: map,
      center: myLatlng,
      radius: dist*1000
    };
	
	//cria o circulo
	var cityCircle = new google.maps.Circle(circleOptions); 
	
	//localizações do eventos, para eliminar as repetidas e posteriormente colocar no title de cada marker todos os eventos que aparecem nessa localização
	var localizacoes = [];
  
	//construir markers com as localizações dos eventos
	for (var i = 0;i<nodeList.length;i++)
	{
		//latitude
		var noEventLat= nodeList[i].getElementsByTagName("latitude")[0];
		var eventLat = noEventLat.childNodes[0].nodeValue;

		//longitude
		var noEventLong= nodeList[i].getElementsByTagName("longitude")[0];
		var eventLong = noEventLong.childNodes[0].nodeValue;

		//nome do event para colocar no título do marker
		var noEventName= nodeList[i].getElementsByTagName("name")[0];
		var eventName = noEventName.childNodes[0].nodeValue;
		
		//array associativo com a key sendo as coordenadas e o value os eventos que irão acontecer nessas coordenadas
		var latlng_aux = eventLat+","+eventLong;
		if (localizacoes[latlng_aux] != null)
		{
			var str_aux= localizacoes[latlng_aux];
			localizacoes[latlng_aux]= str_aux+"\n"+eventName;
		} else {
			localizacoes[latlng_aux]= eventName;
		}
	}
	
	//criar os markers para cada localizações, um marker pode ter vários eventos, sendo estes todos listados no title
	for(key in localizacoes) 
	{ 
		//vai buscar as coordenadas à key do array associativo
		var coord_aux2 = key.split(",");
		var eventLat2 = coord_aux2[0];
		var eventLong2 = coord_aux2[1];
		
		//criar posição do markers
		var eventLatlng = new google.maps.LatLng(eventLat2,eventLong2);
		
		//criar marker
		var marker = new google.maps.Marker({
		  position: eventLatlng,
		  map: map,
		  title: localizacoes[key]
		});
	}	
}

//mostrar o tab dos eventos
function mostrarTabEvents()
{
	document.getElementById("divTagsAndTracks").style.display = 'none';
	document.getElementById("divTagsAndTracks").style.visibility = 'hidden';
	
	//muda a class dos td para mudar a aparência dos td
	document.getElementById("tdEvents").className = 'tdActive';
	document.getElementById("tdTagsTracks").className = 'tdInactive';
	mostrarElem("divEvents");
}

//mostrar o tab das tags e events
function mostrarTabTagsTracks()
{
	document.getElementById("divEvents").style.display = 'none';
	document.getElementById("divEvents").style.visibility = 'hidden';
	
	//muda a class dos td para mudar a aparência dos td
	document.getElementById("tdEvents").className = 'tdInactive';
	document.getElementById("tdTagsTracks").className = 'tdActive';
	mostrarElem("divTagsAndTracks");
}

//activa o select das tags e a textbox com o nº de tracks apos o primeiro click no botão pesquisar
function activarSelectTags()
{
	document.getElementById("stags").removeAttribute("disabled");
	document.getElementById("tntracks").removeAttribute("disabled");
}

//valida o artista
function validarArtista()
{
	//artista introduzido na text box
	var artistaElem=document.getElementById("tartista");
	var artista=artistaElem.value

	//trim para validar mesmo que o valor introduzido sejam vários espaços
	if (artista.trim() != "")
	{
		limparDivTracks();
		getArtistTags();
		activarSelectTags();
	} else {
		alert("Introduza um artista.");
	}
}
//valida o nº de tracks na respectiva textbox e avança com a respectiva pesquisa caso seja um nº válido 
function validarNTracks()
{
	//vai buscar o número de tracks indicadas
	var ntracksElem=document.getElementById("tntracks");
	var ntracks=ntracksElem.value;

	if (ntracks >0 && ntracks < 101)
	{
		loadingImg('divTracks');
		apagarEscolherSelect();
		getTopTracksByTag();
	} else {
		alert("Número de tracks inválido.\nIntroduza um número entre 0 e 101.");
	}
}

//valida o raio na respectiva textbox e avança com a respectiva pesquisa caso seja um nº válido 
function validarRaio()
{
	//vai buscar a localização indicada
	var locElem=document.getElementById("tlocal");
	var loc=locElem.value;

	//vai buscar a distância indicada
	var distElem=document.getElementById("tdist");
	var dist=distElem.value;

	if (loc.trim() != "")
	{
		if (dist >0 && dist < 101)
		{
			loadingImgEvents('divPagEvents');
			getEvents();
		} else {
			alert("Raio inválido.\nIntroduza um raio entre 0 e 101 Km.");
		}
	} else {
		alert("Introduza uma localização.");
	}
}

//apaga a opção "Escolher..." do select após ser escolhida uma tag
function apagarEscolherSelect()
{
	//tag seleciona no select
	var selectTags=document.getElementById("stags");
	var escolherOption = selectTags.options[0].text; //"Escolher..." encontra-se na posição 0 do select
	if (escolherOption == 'Escolher...')
	{
		selectTags.remove(0);
	}



}
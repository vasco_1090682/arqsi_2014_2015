﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IDEIMusic.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace IDEIMusic.DAL
{
    public class EditoraContext : DbContext
    {
        public DbSet<Disco> Discos { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Venda> Vendas { get; set; }
        public DbSet<RegistoVenda> RegistoVendas { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
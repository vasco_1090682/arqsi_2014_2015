﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IDEIMusic.Models;

namespace IDEIMusic.ViewModels
{
    public class VendaIndexData
    {
        public IEnumerable<Venda> Vendas { get; set; }
        public IEnumerable<RegistoVenda> Discos { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IDEIMusic.Models
{
    public class Venda
    {
        public int VendaID { get; set; }

        [ForeignKey("UserProfile")]
        [Display(Name = "Loja")]
        public int UserID { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Data da venda")]
        public DateTime DataVenda { get; set; }

        public virtual UserProfile UserProfile { get; set; }
        public virtual ICollection<RegistoVenda> Discos { get; set; }

        public int TotalDiscos
        {
            get 
            {
                int somaDiscos= 0;
                foreach (var d in Discos)
                {
                    somaDiscos = somaDiscos + d.Quantidade;
                }
                return somaDiscos;
            }
        }

        public double CustoTotal
        {
            get
            {
                double custoTotal = 0;
                foreach (var d in Discos)
                {
                    custoTotal = custoTotal + d.TotalPorDisco;
                }
                return custoTotal;
            }
        }
    }
}
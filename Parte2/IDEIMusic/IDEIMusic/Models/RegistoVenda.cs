﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IDEIMusic.Models
{
    public class RegistoVenda
    {
        public int RegistoVendaID { get; set; }

        [ForeignKey("Venda")]
        public int VendaID { get; set; }

        [ForeignKey("Disco")]
        public int DiscoID { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Quantidade deve ser um número positivo.")]
        public int Quantidade { get; set; }

        public virtual Venda Venda { get; set; }
        public virtual Disco Disco { get; set; }

        public double TotalPorDisco
        {
            get { return (double)(Quantidade * Disco.Preco); }
        }
    }
}
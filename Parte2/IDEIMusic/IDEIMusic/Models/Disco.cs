﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IDEIMusic.Models
{
    public class Disco
    {
        public int DiscoID { get; set; }
        public string Titulo { get; set; }
        public string Artista { get; set; }

        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:0}", ApplyFormatInEditMode = true)]
        [Display(Name = "Preço")]
        public decimal Preco { get; set; }

        [Range(0, int.MaxValue, ErrorMessage = "Quantidade deve ser um número positivo.")]
        public int Quantidade { get; set; }
    }
}
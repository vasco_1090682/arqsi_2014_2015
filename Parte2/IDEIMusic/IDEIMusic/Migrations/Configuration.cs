namespace IDEIMusic.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using IDEIMusic.Models;
    using System.Collections.Generic;
    using WebMatrix.WebData;
    using System.Web.Security;

    internal sealed class Configuration : DbMigrationsConfiguration<IDEIMusic.DAL.EditoraContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(IDEIMusic.DAL.EditoraContext context)
        {
            SeedMembership();

            var discos = new List<Disco>
            {
                new Disco { Titulo = "OK Computer", Artista = "Radiohead", Quantidade = 991, Preco = 12},
                new Disco { Titulo = "The Dark Side of the Moon", Artista = "Pink Floyd", Quantidade = 627, Preco = 11},
                new Disco { Titulo = "The Velvet Underground & Nico", Artista = "The Velvet Underground & Nico", Quantidade = 564, Preco = 12},
                new Disco { Titulo = "Wish You Were Here", Artista = "Pink Floyd", Quantidade = 1042, Preco = 15},
                new Disco { Titulo = "Abbey Road", Artista = "The Beatles", Quantidade = 209, Preco = 12},
                new Disco { Titulo = "In the Court of the Crimson King", Artista = "King Crimson", Quantidade = 468, Preco = 11},
                new Disco { Titulo = "Revolver", Artista = "The Beatles", Quantidade = 899, Preco = 14},
                new Disco { Titulo = "Kid A", Artista = "Radiohead", Quantidade = 1144, Preco = 12},
                new Disco { Titulo = "Led Zeppelin [IV]", Artista = "Led Zeppelin", Quantidade = 379, Preco = 12},
                new Disco { Titulo = "Kind of Blue", Artista = "Miles Davis", Quantidade = 753, Preco = 17},
                new Disco { Titulo = "Loveless", Artista = "My Bloody Valentine", Quantidade = 951, Preco = 16},
                new Disco { Titulo = "Highway 61 Revisited", Artista = "Bob Dylan", Quantidade = 442, Preco = 9}
            };
            discos.ForEach(s => context.Discos.AddOrUpdate(p => p.Titulo, s));
            context.SaveChanges();

            var vendas = new List<Venda>
            {
                new Venda {VendaID = 1, UserID = 3, DataVenda = DateTime.Parse("2014-09-01")},
                new Venda {VendaID = 2, UserID = 3, DataVenda = DateTime.Parse("2014-09-28")},
                new Venda {VendaID = 3, UserID = 4, DataVenda = DateTime.Parse("2014-10-11")}
            };
            vendas.ForEach(v => context.Vendas.AddOrUpdate(p => p.VendaID, v));
            context.SaveChanges();

            var registoVendas = new List<RegistoVenda>
            {
                new RegistoVenda {RegistoVendaID = 1, VendaID = 1, DiscoID = 5, Quantidade = 100},
                new RegistoVenda {RegistoVendaID = 2, VendaID = 1, DiscoID = 6, Quantidade = 25},
                new RegistoVenda {RegistoVendaID = 3, VendaID = 1, DiscoID = 8, Quantidade = 50},
                new RegistoVenda {RegistoVendaID = 4, VendaID = 1, DiscoID = 11, Quantidade = 100},
                new RegistoVenda {RegistoVendaID = 5, VendaID = 1, DiscoID = 12, Quantidade = 50},
                new RegistoVenda {RegistoVendaID = 6, VendaID = 2, DiscoID = 1, Quantidade = 125},
                new RegistoVenda {RegistoVendaID = 7, VendaID = 2, DiscoID = 2, Quantidade = 10},
                new RegistoVenda {RegistoVendaID = 8, VendaID = 2, DiscoID = 3, Quantidade = 100},
                new RegistoVenda {RegistoVendaID = 9, VendaID = 2, DiscoID = 6, Quantidade = 80},
                new RegistoVenda {RegistoVendaID = 10, VendaID = 3, DiscoID = 3, Quantidade = 20},
                new RegistoVenda {RegistoVendaID = 11, VendaID = 3, DiscoID = 5, Quantidade = 45},
                new RegistoVenda {RegistoVendaID = 12, VendaID = 3, DiscoID = 6, Quantidade = 32},
                new RegistoVenda {RegistoVendaID = 13, VendaID = 3, DiscoID = 7, Quantidade = 10},
                new RegistoVenda {RegistoVendaID = 14, VendaID = 3, DiscoID = 8, Quantidade = 30},
                new RegistoVenda {RegistoVendaID = 15, VendaID = 3, DiscoID = 10, Quantidade = 40}
            };
            registoVendas.ForEach(v => context.RegistoVendas.AddOrUpdate(p => p.RegistoVendaID, v));
            context.SaveChanges();
        }

        private void SeedMembership()
        {
            WebSecurity.InitializeDatabaseConnection("EditoraContext",
                "UserProfile", "UserId", "UserName", autoCreateTables: true);

            var roles = (SimpleRoleProvider)System.Web.Security.Roles.Provider;
            var membership = (SimpleMembershipProvider)Membership.Provider;

            //Criar os perfis
            if (!roles.RoleExists("Admin"))
            {
                roles.CreateRole("Admin");
            }
            if (!roles.RoleExists("Gestor"))
            {
                roles.CreateRole("Gestor");
            }
            if (!roles.RoleExists("Loja"))
            {
                roles.CreateRole("Loja");
            }

            //Criar utilzadores
            if (membership.GetUser("pyro", false) == null)
            {
                WebSecurity.CreateUserAndAccount("pyro", "xpto123", new { UserEmail = "pyro@pyro.land", UserAPIKey = System.Guid.NewGuid().ToString() });
            }

            if (membership.GetUser("medic", false) == null)
            {
                WebSecurity.CreateUserAndAccount("medic", "xpto123", new { UserEmail = "medic@oktober.fest", UserAPIKey = System.Guid.NewGuid().ToString() });
            }

            if (membership.GetUser("sniper", false) == null)
            {
                WebSecurity.CreateUserAndAccount("sniper", "xpto123", new { UserEmail = "sniper@jungle.au", UserAPIKey = System.Guid.NewGuid().ToString() });
            }

            if (membership.GetUser("heavy", false) == null)
            {
                WebSecurity.CreateUserAndAccount("heavy", "xpto123", new { UserEmail = "heavy@mother.ru", UserAPIKey = System.Guid.NewGuid().ToString() });
            }

            //Atribuir perfis ao utilizadores
            if (!roles.GetRolesForUser("pyro").Contains("Admin"))
            {
                roles.AddUsersToRoles(new[] { "pyro" }, new[] { "Admin" });
            }

            if (!roles.GetRolesForUser("medic").Contains("Gestor"))
            {
                roles.AddUsersToRoles(new[] { "medic" }, new[] { "Gestor" });
            }

            if (!roles.GetRolesForUser("sniper").Contains("Loja"))
            {
                roles.AddUsersToRoles(new[] { "sniper" }, new[] { "Loja" });
            }

            if (!roles.GetRolesForUser("heavy").Contains("Loja"))
            {
                roles.AddUsersToRoles(new[] { "heavy" }, new[] { "Loja" });
            }
        }
    }
}

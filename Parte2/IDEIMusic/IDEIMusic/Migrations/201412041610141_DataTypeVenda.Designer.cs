// <auto-generated />
namespace IDEIMusic.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class DataTypeVenda : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(DataTypeVenda));
        
        string IMigrationMetadata.Id
        {
            get { return "201412041610141_DataTypeVenda"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

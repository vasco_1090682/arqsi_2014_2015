namespace IDEIMusic.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CriarTabelasVendasV2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Venda",
                c => new
                    {
                        VendaID = c.Int(nullable: false, identity: true),
                        UserID = c.Int(nullable: false),
                        DataVenda = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.VendaID)
                .ForeignKey("dbo.UserProfile", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.RegistoVenda",
                c => new
                    {
                        RegistoVendaID = c.Int(nullable: false, identity: true),
                        VendaID = c.Int(nullable: false),
                        DiscoID = c.Int(nullable: false),
                        Quantidade = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RegistoVendaID)
                .ForeignKey("dbo.Venda", t => t.VendaID, cascadeDelete: true)
                .ForeignKey("dbo.Disco", t => t.DiscoID, cascadeDelete: true)
                .Index(t => t.VendaID)
                .Index(t => t.DiscoID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.RegistoVenda", new[] { "DiscoID" });
            DropIndex("dbo.RegistoVenda", new[] { "VendaID" });
            DropIndex("dbo.Venda", new[] { "UserID" });
            DropForeignKey("dbo.RegistoVenda", "DiscoID", "dbo.Disco");
            DropForeignKey("dbo.RegistoVenda", "VendaID", "dbo.Venda");
            DropForeignKey("dbo.Venda", "UserID", "dbo.UserProfile");
            DropTable("dbo.RegistoVenda");
            DropTable("dbo.Venda");
        }
    }
}

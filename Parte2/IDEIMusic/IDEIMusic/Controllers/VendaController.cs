﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IDEIMusic.Models;
using IDEIMusic.DAL;
using IDEIMusic.ViewModels;

namespace IDEIMusic.Controllers
{
    [Authorize(Roles = "Admin")]
    public class VendaController : Controller
    {
        private EditoraContext db = new EditoraContext();

        //
        // GET: /Venda/

        public ActionResult Index(int? id, string sortOrder, string sortOrderDiscos, string volumeStr, string selectVolume, string dataInicio, string dataFim)
        {
            var viewModel = new VendaIndexData();
            viewModel.Vendas = db.Vendas
                .Include(i => i.Discos)
                .Include(i => i.UserProfile);

            if (id != null)
            {
                ViewBag.VendaID = id.Value;
                viewModel.Discos = viewModel.Vendas.Where(i => i.VendaID == id.Value).Single().Discos;
            }
            

            //Search
            //search
            if (!String.IsNullOrEmpty(volumeStr))
            {
                try
                {
                    int volume = Int32.Parse(volumeStr);
                    if (selectVolume == "maiorigual")
                    {
                        viewModel.Vendas = viewModel.Vendas.Where(s => s.TotalDiscos >= volume);
                    }
                    else
                    {
                        viewModel.Vendas = viewModel.Vendas.Where(s => s.TotalDiscos <= volume);
                    }
                    ViewBag.Volume = volumeStr; //guardar o filtro atual para não haver reset à tabela quando se clica em novos sortings
                    ViewBag.SelectVolume = selectVolume; //guardar o filtro atual para não haver reset à tabela quando se clica em novos sortings
                } catch (Exception ex){
                    ViewBag.ErrorVolume = "Introduza um número inteiro. ";
                }
                
            }

            if (!String.IsNullOrEmpty(dataInicio) && !String.IsNullOrEmpty(dataFim))
            {
                try
                {
                    DateTime dt1 = Convert.ToDateTime(dataInicio);
                    DateTime dt2 = Convert.ToDateTime(dataFim);
                    viewModel.Vendas = viewModel.Vendas.Where(s => s.DataVenda > dt1 && s.DataVenda < dt2);
                    ViewBag.DataInicio = dataInicio; //guardar o filtro atual para não haver reset à tabela quando se clica em novos sortings
                    ViewBag.DataFim = dataFim; //guardar o filtro atual para não haver reset à tabela quando se clica em novos sortings
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorData = "Introduza as datas no formato YYYY/MM/DD. ";
                }

            }

            //Sorting
            ViewBag.VendaSortParm = String.IsNullOrEmpty(sortOrder) ? "Venda_desc" : "";
            ViewBag.LojaSortParm = sortOrder == "Loja" ? "Loja_desc" : "Loja";
            ViewBag.DataSortParm = sortOrder == "Data" ? "Data_desc" : "Data";
            ViewBag.TotalDiscosSortParm = sortOrder == "TotalDiscos" ? "TotalDiscos_desc" : "TotalDiscos";
            ViewBag.CustoTotalSortParm = sortOrder == "CustoTotal" ? "CustoTotal_desc" : "CustoTotal";
            ViewBag.CurrentOrder = sortOrder; //guarda a order atual da tabela das vendas para quando na view se clicar na action mostrar ou ordenar a tabela discos pelos cabeçalhos a tabela vendas mantenha a ordenação actual
            switch (sortOrder)
            {
                case "Venda_desc":
                    viewModel.Vendas = viewModel.Vendas.OrderByDescending(s => s.VendaID);
                    break;
                case "Loja":
                    viewModel.Vendas = viewModel.Vendas.OrderBy(s => s.UserProfile.UserName);
                    break;
                case "Loja_desc":
                    viewModel.Vendas = viewModel.Vendas.OrderByDescending(s => s.UserProfile.UserName);
                    break;
                case "Data":
                    viewModel.Vendas = viewModel.Vendas.OrderBy(s => s.DataVenda);
                    break;
                case "Data_desc":
                    viewModel.Vendas = viewModel.Vendas.OrderByDescending(s => s.DataVenda);
                    break;
                case "TotalDiscos":
                    viewModel.Vendas = viewModel.Vendas.OrderBy(s => s.TotalDiscos);
                    break;
                case "TotalDiscos_desc":
                    viewModel.Vendas = viewModel.Vendas.OrderByDescending(s => s.TotalDiscos);
                    break;
                case "CustoTotal":
                    viewModel.Vendas = viewModel.Vendas.OrderBy(s => s.CustoTotal);
                    break;
                case "CustoTotal_desc":
                    viewModel.Vendas = viewModel.Vendas.OrderByDescending(s => s.CustoTotal);
                    break;
                default:
                    viewModel.Vendas = viewModel.Vendas.OrderBy(s => s.VendaID);
                    break;
            }

            ViewBag.TituloSortParm = String.IsNullOrEmpty(sortOrderDiscos) ? "Titulo_desc" : "";
            ViewBag.ArtistaSortParm = sortOrderDiscos == "Artista" ? "Artista_desc" : "Artista";
            ViewBag.QuantidadeSortParm = sortOrderDiscos == "Quantidade" ? "Quantidade_desc" : "Quantidade";
            ViewBag.CustoTotalPorDiscoSortParm = sortOrderDiscos == "CustoTotalPorDisco" ? "CustoTotalPorDisco_desc" : "CustoTotalPorDisco";
            ViewBag.CurrentDiscosOrder = sortOrderDiscos;
            if (viewModel.Discos != null){
                switch (sortOrderDiscos)
                {
                    case "Titulo_desc":
                        viewModel.Discos = viewModel.Discos.OrderByDescending(d => d.Disco.Titulo);
                        break;
                    case "Artista":
                        viewModel.Discos = viewModel.Discos.OrderBy(d => d.Disco.Artista);
                        break;
                    case "Artista_desc":
                        viewModel.Discos = viewModel.Discos.OrderByDescending(d => d.Disco.Artista);
                        break;
                    case "Quantidade":
                        viewModel.Discos = viewModel.Discos.OrderBy(d => d.Quantidade);
                        break;
                    case "Quantidade_desc":
                        viewModel.Discos = viewModel.Discos.OrderByDescending(d => d.Quantidade);
                        break;
                    case "CustoTotalPorDisco":
                        viewModel.Discos = viewModel.Discos.OrderBy(d => d.TotalPorDisco);
                        break;
                    case "CustoTotalPorDisco_desc":
                        viewModel.Discos = viewModel.Discos.OrderByDescending(d => d.TotalPorDisco);
                        break;
                    default:
                        viewModel.Discos = viewModel.Discos.OrderBy(d => d.Disco.Titulo);
                        break;
                }
            }

            return View(viewModel);

            //var vendas = db.Vendas.Include(v => v.UserProfile);
            //return View(vendas.ToList());
        }

        //
        // GET: /Venda/Details/5
        [NonAction] //restringir acesso
        public ActionResult Details(int id = 0)
        {
            Venda venda = db.Vendas.Find(id);
            if (venda == null)
            {
                return HttpNotFound();
            }
            return View(venda);
        }

        //
        // GET: /Venda/Create
        [NonAction] //restringir acesso
        public ActionResult Create()
        {
            ViewBag.UserID = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /Venda/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Venda venda)
        {
            if (ModelState.IsValid)
            {
                db.Vendas.Add(venda);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserID = new SelectList(db.UserProfiles, "UserId", "UserName", venda.UserID);
            return View(venda);
        }

        //
        // GET: /Venda/Edit/5
        [NonAction] //restringir acesso
        public ActionResult Edit(int id = 0)
        {
            Venda venda = db.Vendas.Find(id);
            if (venda == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserID = new SelectList(db.UserProfiles, "UserId", "UserName", venda.UserID);
            return View(venda);
        }

        //
        // POST: /Venda/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Venda venda)
        {
            if (ModelState.IsValid)
            {
                db.Entry(venda).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserID = new SelectList(db.UserProfiles, "UserId", "UserName", venda.UserID);
            return View(venda);
        }

        //
        // GET: /Venda/Delete/5
        [NonAction] //restringir acesso
        public ActionResult Delete(int id = 0)
        {
            Venda venda = db.Vendas.Find(id);
            if (venda == null)
            {
                return HttpNotFound();
            }
            return View(venda);
        }

        //
        // POST: /Venda/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Venda venda = db.Vendas.Find(id);
            db.Vendas.Remove(venda);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
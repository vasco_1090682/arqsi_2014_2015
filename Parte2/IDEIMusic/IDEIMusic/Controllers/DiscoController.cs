﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IDEIMusic.Models;
using IDEIMusic.DAL;
using PagedList;

namespace IDEIMusic.Controllers
{
    public class DiscoController : Controller
    {
        private EditoraContext db = new EditoraContext();

        //
        // GET: /Disco/

        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;

            ViewBag.TituloSortParm = String.IsNullOrEmpty(sortOrder) ? "Titulo_desc" : "";
            ViewBag.ArtistaSortParm = sortOrder == "Artista" ? "Artista_desc" : "Artista";
            ViewBag.PrecoSortParm = sortOrder == "Preco" ? "Preco_desc" : "Preco";
            ViewBag.QuantidadeSortParm = sortOrder == "Quantidade" ? "Quantidade_desc" : "Quantidade";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var discos = from d in db.Discos select d;

            if (!String.IsNullOrEmpty(searchString))
            {
                discos = discos.Where(s => s.Titulo.ToUpper().Contains(searchString.ToUpper())
                                       || s.Artista.ToUpper().Contains(searchString.ToUpper()));
            }

            switch (sortOrder)
            {
                case "Titulo_desc":
                    discos = discos.OrderByDescending(s => s.Titulo);
                    break;
                case "Artista":
                    discos = discos.OrderBy(s => s.Artista);
                    break;
                case "Artista_desc":
                    discos = discos.OrderByDescending(s => s.Artista);
                    break;
                case "Preco":
                    discos = discos.OrderBy(s => s.Preco);
                    break;
                case "Preco_desc":
                    discos = discos.OrderByDescending(s => s.Preco);
                    break;
                case "Quantidade":
                    discos = discos.OrderBy(s => s.Quantidade);
                    break;
                case "Quantidade_desc":
                    discos = discos.OrderByDescending(s => s.Quantidade);
                    break;
                default:
                    discos = discos.OrderBy(s => s.Titulo);
                    break;
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(discos.ToPagedList(pageNumber, pageSize));
        }

        //
        // GET: /Disco/Details/5

        public ActionResult Details(int id = 0)
        {
            Disco disco = db.Discos.Find(id);
            if (disco == null)
            {
                return HttpNotFound();
            }
            return View(disco);
        }

        //
        // GET: /Disco/Create
        [Authorize(Roles = "Gestor")]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Disco/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Titulo, Artista, Quantidade, Preco")]Disco disco)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Discos.Add(disco);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }                
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name after DataException and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return View(disco);
        }

        //
        // GET: /Disco/Edit/5
        [Authorize(Roles = "Gestor")]
        public ActionResult Edit(int id = 0)
        {
            Disco disco = db.Discos.Find(id);
            if (disco == null)
            {
                return HttpNotFound();
            }
            return View(disco);
        }

        //
        // POST: /Disco/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DiscoID, Titulo, Artista, Quantidade, Preco")]Disco disco)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(disco).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name after DataException and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return View(disco);
        }

        //
        // GET: /Disco/Delete/5
        [Authorize(Roles = "Gestor")]
        public ActionResult Delete(bool? saveChangesError = false, int id = 0)
        {
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Delete failed. Try again, and if the problem persists see your system administrator.";
            }
            Disco disco = db.Discos.Find(id);
            if (disco == null)
            {
                return HttpNotFound();
            }
            return View(disco);
        }

        //
        // POST: /Disco/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Disco disco = db.Discos.Find(id);
                db.Discos.Remove(disco);
                db.SaveChanges();
            }
            catch (DataException/* dex */)
            {
                // uncomment dex and log error. 
                return RedirectToAction("Delete", new { id = id, saveChangesError = true });
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IDEIMusic.Models;
using IDEIMusic.DAL;
using WebMatrix.WebData;
using System.Web.Security;

namespace IDEIMusic.Controllers
{
    [Authorize(Roles = "Admin")]
    public class LojaController : Controller
    {
        private EditoraContext db = new EditoraContext();

        //
        // GET: /Loja/

        public ActionResult Index(string sortOrder, string searchString)
        {
            //mostrar apenas os UserProfiles de lojas
            //lista auxiliar com os users
            var users_aux = db.UserProfiles.ToList();

            var roles = (SimpleRoleProvider)System.Web.Security.Roles.Provider;

            //a mesma lista de users de onde vão ser removidos os utilizadores Admin e Gestor enquanto se itera a lista auxiliar
            var users = db.UserProfiles.ToList();

            foreach (var u in users_aux)
            {
                if (roles.IsUserInRole(u.UserName, "Admin") || roles.IsUserInRole(u.UserName, "Gestor"))
                {
                    users.Remove(u);
                }
            }

            var lojas = from d in users select d;

            //search
            if (!String.IsNullOrEmpty(searchString))
            {
                lojas = lojas.Where(s => s.UserName.ToUpper().Contains(searchString.ToUpper()));
            }

            //Sorting
            ViewBag.LojaSortParm = String.IsNullOrEmpty(sortOrder) ? "Loja_desc" : "";
            ViewBag.EmailSortParm = sortOrder == "Email" ? "Email_desc" : "Email";
            ViewBag.APIKeySortParm = sortOrder == "APIKey" ? "APIKey_desc" : "APIKey";
            
            switch (sortOrder)
            {
                case "Loja_desc":
                    lojas = lojas.OrderByDescending(s => s.UserName);
                    break;
                case "Email":
                    lojas = lojas.OrderBy(s => s.UserEmail);
                    break;
                case "Email_desc":
                    lojas = lojas.OrderByDescending(s => s.UserEmail);
                    break;
                case "APIKey":
                    lojas = lojas.OrderBy(s => s.UserAPIKey);
                    break;
                case "APIKey_desc":
                    lojas = lojas.OrderByDescending(s => s.UserAPIKey);
                    break;
                default:
                    lojas = lojas.OrderBy(s => s.UserName);
                    break;
            }

            return View(lojas.ToList());
            //return View(users);
        }

        //
        // GET: /Loja/Details/5

        public ActionResult Details(int id = 0)
        {
            UserProfile userprofile = db.UserProfiles.Find(id);
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            return View(userprofile);
        }

        //
        // GET: /Loja/Create
        [NonAction] //restringir acesso
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Loja/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserProfile userprofile)
        {
            if (ModelState.IsValid)
            {
                db.UserProfiles.Add(userprofile);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(userprofile);
        }

        //
        // GET: /Loja/Edit/5
        [NonAction] //restringir acesso
        public ActionResult Edit(int id = 0)
        {
            UserProfile userprofile = db.UserProfiles.Find(id);
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            return View(userprofile);
        }

        //
        // POST: /Loja/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserProfile userprofile)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userprofile).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(userprofile);
        }

        //
        // GET: /Loja/Delete/5
        [NonAction] //restringir acesso
        public ActionResult Delete(int id = 0)
        {
            UserProfile userprofile = db.UserProfiles.Find(id);
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            return View(userprofile);
        }

        //
        // POST: /Loja/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserProfile userprofile = db.UserProfiles.Find(id);
            db.UserProfiles.Remove(userprofile);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
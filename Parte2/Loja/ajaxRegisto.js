/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var xmlHttpObj;
//Cria objecto XML HTTP
function CreateXmlHttpRequestObject(){ 
    xmlHttpObj=null;
    if (window.XMLHttpRequest)
        xmlHttpObj=new XMLHttpRequest();
        else if (window.ActiveXObject)
            xmlHttpObj=new ActiveXObject("Microsoft.XMLHTTP");
    return xmlHttpObj;
}

function MakeXMLHTTPCall() {
    xmlHttpObj = CreateXmlHttpRequestObject();
    var usr = document.getElementById("usrname").value;
    var pwd = document.getElementById("pwd").value;
    var url = "dal/dal.php?createuser="+usr+"&password="+pwd;
    if (xmlHttpObj){
        // Definição do URL para efectuar pedido HTTP - método GET
        xmlHttpObj.open("GET",url,true);
        // Registo do EventHandler
        xmlHttpObj.onreadystatechange = stateHandler;
        xmlHttpObj.send(null);
    }
}

function stateHandler() {

    if ( xmlHttpObj.readyState == 4 && xmlHttpObj.status == 200) { // resposta do servidor completa
// recebe resposta
    var resposta = xmlHttpObj.responseText;
    //alert(resposta);
    if(resposta == "OK") {
        var erro = document.getElementById("msgErro");
        erro.style.visibility ="visible";
        erro.innerHTML = "Criado!";        
        window.location.replace("main.php");
    }
    else if(resposta =="NO")
    {
        var erro = document.getElementById("msgErro");
        erro.style.visibility ="visible";
        erro.innerHTML = "Utilizador ja existe!";
    }
    else
    {
        var erro = document.getElementById("msgErro");
        erro.style.visibility ="visible";
        erro.innerHTML = "Sem ligação ao servidor!";
    }
    }
}

function validaRegisto(){

    var usr = document.getElementById("usrname").value;
    var pwd = document.getElementById("pwd").value;
    var erro = document.getElementById("msgErro");

    if(usr == "" || pwd == "") {
        erro.style.visibility = "visible";
        erro.innerHTML = "ERRO: Falta nome de utilizador e/ou password!";
    }
        else MakeXMLHTTPCall();
}
<?
require_once ("../admin/Disco.php");
require_once ("../admin/DiscoDAL.php");
require_once ("../admin/credenciaisAutenticacao.php");

$dal = new DiscoDAL();
$sql = "SELECT * FROM Disco";
$discos = $dal->query($sql);

if ($discos!=null)
{
	$xml = '<?xml version="1.0" encoding="UTF-8"?>';
	$xml .= "\n<catalogo>\n";
		
	for ($i = 0; $i < sizeof($discos); $i++)
	{
		$xml .= "<disco>\n";
		$xml .= "<discoid>".$discos[$i]->DiscoID."</discoid>\n";
		$xml .= "<titulo>".xml_entities($discos[$i]->Titulo)."</titulo>\n";
		$xml .= "<artista>".xml_entities($discos[$i]->Artista)."</artista>\n";
		$xml .= "<quantidade>".$discos[$i]->Quantidade."</quantidade>\n";
		$xml .= "<preco>".$discos[$i]->Preco."</preco>\n";
		$xml .= "</disco>\n";
	}

	$xml .= "</catalogo>";
	Header('Content-type: text/xml');
	echo $xml;
} else {
	echo "Cat�logo vazio.";
}


//m�todos auxiliares
//substituir caracteres especiais
function xml_entities($string) {
    return str_replace(
        array("&",     "<",    ">",    '"',      "'"),
        array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"), 
        $string
    );
}
?>
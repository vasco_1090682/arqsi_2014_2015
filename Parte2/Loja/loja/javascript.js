var xmlHttpObj=null;
var request=null; //variável que vai indicar no stateHandler que código deve correr
var numDiscos = 0;
discos=[];
function CreateXmlHttpRequestObject( )
{ 
  if (window.XMLHttpRequest)
  {
      xmlHttpObj=new XMLHttpRequest();
  }
  else if (window.ActiveXObject)
  {
    xmlHttpObj=new ActiveXObject("Microsoft.XMLHTTP");
  }
  return xmlHttpObj;
}

function getCatalogo()
{     
	xmlHttpObj = CreateXmlHttpRequestObject();
	
	// efectuar pedido Ajax ( open, registar eventHandler, send , etc)
	if (xmlHttpObj)
	{
    // Definição do URL para efectuar pedido HTTP - método GET
    xmlHttpObj.open("GET","getCatalogoFromDB.php", true);
	
    // Registo do EventHandler
    request="getCatalogo";
    xmlHttpObj.onreadystatechange = stateHandler;
    xmlHttpObj.send(null);
	}
}

function stateHandler()
{ 
  if ( xmlHttpObj.readyState == 4 && xmlHttpObj.status == 200)
  { 
    if (request=="getCatalogo")
    {
		var docxml = xmlHttpObj.responseXML;
		var nodeList = docxml.getElementsByTagName("disco");
		construirCatalogo(nodeList);
    }
  }
}

function construirCatalogo(nodeList)
{
	var str = "<table>";
	str += "<tr><th>DiscoID</th><th>Título</th><th>Artista</th><th>Quantidade</th><th>Preço</th></tr>";
	for (i=0;i<nodeList.length;i++)
	{
		str += "<tr>";
		
		var noDiscoID= nodeList[i].getElementsByTagName("discoid")[0];
		var discoID = noDiscoID.childNodes[0].nodeValue;
		str += "<td>"+ discoID +"</td>";
		
		var noTitulo= nodeList[i].getElementsByTagName("titulo")[0];
		var titulo = noTitulo.childNodes[0].nodeValue;
		str += "<td>"+ titulo +"</td>";
		
		var noArtista= nodeList[i].getElementsByTagName("artista")[0];
		var artista = noArtista.childNodes[0].nodeValue;
		str += "<td>"+ artista +"</td>";
		
		var noQuantidade = nodeList[i].getElementsByTagName("quantidade")[0];
		var quantidade = noQuantidade.childNodes[0].nodeValue;
		str += "<td>"+ quantidade +"</td>";
		
		var noPreco= nodeList[i].getElementsByTagName("preco")[0];
		var preco = noPreco.childNodes[0].nodeValue;
		str += "<td>"+ preco +"</td>";
		
		str += "</tr>";
	}
	str += "</table>";

	// alteração do conteúdo textual do div
	document.getElementById("divDiscos").innerHTML = str;
}
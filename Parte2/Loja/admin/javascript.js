var xmlHttpObj=null;
var request=null; //variável que vai indicar no stateHandler que código deve correr
var numDiscos = 0;
discos=[];
function CreateXmlHttpRequestObject( )
{ 
  if (window.XMLHttpRequest)
  {
      xmlHttpObj=new XMLHttpRequest();
  }
  else if (window.ActiveXObject)
  {
    xmlHttpObj=new ActiveXObject("Microsoft.XMLHTTP");
  }
  return xmlHttpObj;
}

function getAPIKey()
{     
	xmlHttpObj = CreateXmlHttpRequestObject();
	
	// efectuar pedido Ajax ( open, registar eventHandler, send , etc)
	if (xmlHttpObj)
	{
    // Definição do URL para efectuar pedido HTTP - método GET
    xmlHttpObj.open("GET","clientWSgetAPIKey.php", true);

    // Registo do EventHandler
    request="getAPIKey";
    xmlHttpObj.onreadystatechange = stateHandler;
    xmlHttpObj.send(null);
	}
}

function getCatalogo()
{     
	xmlHttpObj = CreateXmlHttpRequestObject();
	
	// efectuar pedido Ajax ( open, registar eventHandler, send , etc)
	if (xmlHttpObj)
	{
    // Definição do URL para efectuar pedido HTTP - método GET
    xmlHttpObj.open("GET","clientWSgetCatalogo.php", true);
	
    // Registo do EventHandler
    request="getCatalogo";
    xmlHttpObj.onreadystatechange = stateHandler;
    xmlHttpObj.send(null);
	}
}

function registarEncomenda()
{     
	xmlHttpObj = CreateXmlHttpRequestObject();
	
	// efectuar pedido Ajax ( open, registar eventHandler, send , etc)
	if (xmlHttpObj)
	{
    // Definição do URL para efectuar pedido HTTP - método GET
	// Guardar os discos a encomendar (os que têm quantidade indicada)
	var j = 0;
	for (i = 0; i<numDiscos;i++)
	{
		 var qtd=document.getElementsByName("tQtd"+i)[0].value;
		 if (qtd > 0 && qtd < 9999)
		 {
			var attID = document.getElementsByName("tQtd"+i)[0].getAttribute("id");
			att_aux = attID.split("tQtd");
			attID = att_aux[1];
			discos[j] = attID+";"+qtd;
			j++;
		 }
	}
    xmlHttpObj.open("GET","clientWSregistarVenda.php?discos="+discos, true);
	
    // Registo do EventHandler
    request="registarEncomenda";
    xmlHttpObj.onreadystatechange = stateHandler;
    xmlHttpObj.send(null);
	}
}

function stateHandler()
{ 
  if ( xmlHttpObj.readyState == 4 && xmlHttpObj.status == 200)
  { 
    if (request == "getAPIKey")
    {
      // recebe resposta
      var respostaText = xmlHttpObj.responseText;
	  document.getElementById("tapikey").value = respostaText;
    } else if (request=="getCatalogo")
    {
      var docxml = xmlHttpObj.responseXML;
      var nodeList = docxml.getElementsByTagName("disco");
      construirCatalogo(nodeList);
    } else if (request=="registarEncomenda")
	{
		var respostaText = xmlHttpObj.responseText;
		if (respostaText == "discos ok"){
			alert("Encomenda registada com sucesso.");
			document.getElementById("divDiscos").innerHTML = "";
			numDiscos = 0; //reiniciar o contador dos discos porque o catalogo é apagado da página
		} else {
			alert(respostaText);
		}
	}
  }
}

function construirCatalogo(nodeList)
{
	numDiscos = nodeList.length;
	var str = "<table>";
	str += "<tr><th>DiscoID</th><th>Título</th><th>Artista</th><th>Quantidade</th><th>Preço</th><th>Quantidade a encomendar</th></tr>";
	for (i=0;i<nodeList.length;i++)
	{
		str += "<tr>";
		
		var noDiscoID= nodeList[i].getElementsByTagName("discoid")[0];
		var discoID = noDiscoID.childNodes[0].nodeValue;
		str += "<td>"+ discoID +"</td>";
		
		var noTitulo= nodeList[i].getElementsByTagName("titulo")[0];
		var titulo = noTitulo.childNodes[0].nodeValue;
		str += "<td>"+ titulo +"</td>";
		
		var noArtista= nodeList[i].getElementsByTagName("artista")[0];
		var artista = noArtista.childNodes[0].nodeValue;
		str += "<td>"+ artista +"</td>";
		
		var noQuantidade = nodeList[i].getElementsByTagName("quantidade")[0];
		var quantidade = noQuantidade.childNodes[0].nodeValue;
		str += "<td>"+ quantidade +"</td>";
		
		var noPreco= nodeList[i].getElementsByTagName("preco")[0];
		var preco = noPreco.childNodes[0].nodeValue;
		str += "<td>"+ preco +"</td>";
		
		str += "<td><input name='tQtd"+i+"' id='tQtd"+discoID+"' size='10' type='text'></td>";
		str += "</tr>";
	}
	str += "</table>";

	// alteração do conteúdo textual do div
	document.getElementById("divDiscos").innerHTML = str;
}


function validarCatalogo()
{
	if (numDiscos > 0)
	{
		validarTextBoxes();
	} else {
		alert("Catálogo vazio.");
	}
}

function validarTextBoxes()
{
	var txtBoxesValidas = 0;
	var txtBoxesVazias = 0;
	for (i = 0; i<numDiscos;i++)
	{
		 var qtd=document.getElementsByName("tQtd"+i)[0].value;
		 if ((qtd > 0 && qtd < 9999))
		 {
			txtBoxesValidas++;
		 } else if (qtd == "" )
		 {
			txtBoxesValidas++;
			txtBoxesVazias++;
		 }
	}

	if (txtBoxesValidas != numDiscos)
	{
		alert("Introduza números de 1 a 9999 ou deixe as caixas de texto em branco.");
	} else if (txtBoxesVazias == numDiscos) {
		alert("Não introduziu quantidade em nenhum disco.");
	} else {
		alert("Registando a encomenda.");
		registarEncomenda();
	}
	
}
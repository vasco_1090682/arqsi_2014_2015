<?php
class Disco{
	public $DiscoID;
	public $Titulo;
	public $Artista;
	public $Quantidade;
	public $Preco;
	
	function __construct($DiscoID, $Titulo, $Artista, $Quantidade, $Preco) {
		$this->DiscoID = $DiscoID;
		$this->Titulo = $Titulo;
		$this->Artista = $Artista;
		$this->Quantidade = $Quantidade;
		$this->Preco = $Preco;
	}
}
?>
<?
if (!session_id()) session_start();
if (!$_SESSION['adminlogon']){ 
    header("Location:../login.php");
    die();
}
?>
<html>
  <head>
    <title>ARQSI- Loja de Discos - Painel Administrador</title>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
	<meta content="utf-8" http-equiv="encoding">
    <script type="text/javascript" src="javascript.js"></script>
	</head>
	<body>
		<table>

			<caption style="font-size: 22pt; margin: 20px;">Painel Administrador</caption>
			<tbody>
			<tr>
				<td>API Key:</td>
				<td><input name="tapikey" id="tapikey" size="50" type="text" disabled></td>
				<td><input value="Obter API Key" type="submit" onClick=getAPIKey();></td>
			</tr>
			</tbody>
		</table>
		<p>
			<input value="Obter Catálogo" type="submit" onClick=getCatalogo();>
			<input value="Registar Encomenda" type="submit" onClick=validarCatalogo();>
		</p>
		<div id="divDiscos"></div>
	</body>
</html>
<?
//obter a apikey from db
include 'getAPIKeyFromDB.php';
$apikey = getAPIKey();

//limpar o cached .wsdl para que mostre updates feitos ao webservice
ini_set("soap.wsdl_cache_enabled", "0");
$client= new SoapClient('http://wvm120.dei.isep.ipp.pt/EditoraWS/Service.svc?wsdl');

//M�todo para retornar todos os discos da base de dados se a API Key for v�lida
//$apikey = '5d0700bc-c63b-46cb-9322-8a692639500f';
if($apikey!="")
{
	$paramsGetDiscos = array('APIKey'=>$apikey);
	$result=$client->getDiscos($paramsGetDiscos);
	$discos=$result->getDiscosResult->Disco;
	
	//construir xml
	$xml = '<?xml version="1.0" encoding="UTF-8"?>';
	$xml .= "\n<catalogo>\n";
	
	for ($i = 0; $i < sizeof($discos); $i++)
	{
		$xml .= "<disco>\n";
		$xml .= "<discoid>".$discos[$i]->DiscoID."</discoid>\n";
		$xml .= "<titulo>".xml_entities($discos[$i]->Titulo)."</titulo>\n";
		$xml .= "<artista>".xml_entities($discos[$i]->Artista)."</artista>\n";
		$xml .= "<quantidade>".$discos[$i]->Quantidade."</quantidade>\n";
		$xml .= "<preco>".$discos[$i]->Preco."</preco>\n";
		$xml .= "</disco>\n";
		//echo $discos[$i]->DiscoID . " - " . $discos[$i]->Titulo . " - " . $discos[$i]->Artista . " - " . $discos[$i]->Quantidade . " - " . $discos[$i]->Preco;
		//echo "<br>";
	}
	$xml .= "</catalogo>";
	Header('Content-type: text/xml');
	echo $xml;
} else {
	echo "Get an API Key";
}

//m�todos auxiliares
//substituir caracteres especiais
function xml_entities($string) {
    return str_replace(
        array("&",     "<",    ">",    '"',      "'"),
        array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"), 
        $string
    );
}
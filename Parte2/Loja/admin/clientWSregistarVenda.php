<?
//obter array com os IDs e quantidades dos discos a encomendar
$json = $_REQUEST['discos'];
$idQtd = explode(",", $json);
$str = "";
for ($i=0;$i<sizeof($idQtd);$i++)
{
	$str .= $idQtd[$i]."\n";
}

//obter a apikey from db
include 'getAPIKeyFromDB.php';
$apikey = getAPIKey();

//limpar o cached .wsdl para que mostre updates feitos ao webservice
ini_set("soap.wsdl_cache_enabled", "0");
$client= new SoapClient('http://wvm120.dei.isep.ipp.pt/EditoraWS/Service.svc?wsdl');
$discos = array();
if($apikey!="")
{
	for ($i=0;$i<sizeof($idQtd);$i++)
	{
		$var_aux = explode(";",$idQtd[$i]);
		$id = $var_aux[0];
		$qtd = $var_aux[1];
		$d = array('DiscoID'=>$id, 'Titulo'=>'', 'Artista'=>'', 'Quantidade'=>$qtd, 'Preco'=>null);
		$discos[]=$d;
	}
	$paramsRegistoVenda = array('APIKey'=>$apikey, 'discos'=>$discos);
	$result=$client->registarVenda($paramsRegistoVenda);
	echo $result->registarVendaResult;
	
	//guardar na base de dados da loja se a encomenda tiver sido efectuada
	if ($result->registarVendaResult == "discos ok")
	{
		require_once ("Disco.php");
		require_once ("DiscoDAL.php");
		require_once ("credenciaisAutenticacao.php");
		for ($i=0;$i<sizeof($idQtd);$i++)
		{
			$var_aux = explode(";",$idQtd[$i]);
			$id = $var_aux[0];
			$qtd = $var_aux[1];
			
			//usar webservice para obter os dados de cada disco
			$paramsDisco = array('APIKey'=>$apikey, 'DiscoID'=>$id);
			$result=$client->getDisco($paramsDisco);
			$disco = $result->getDiscoResult;
			
			//query para guardar na bd da loja
			$d = new Disco($disco->DiscoID,$disco->Titulo,$disco->Artista,$qtd,$disco->Preco);
			$dal = new DiscoDAL();
			$dal->insertOrUpdateDisco($d);
		}
	}
} else {
	echo "Get an API Key";
}
?>
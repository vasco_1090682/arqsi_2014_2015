<?
//rawurlencode para poder enviar parametros com caracteres especiais
$location=rawurlencode($_GET['location']); //localização

//constroi url para consultar a api do musicbrainz
$url = "https://maps.googleapis.com/maps/api/geocode/json?address=".$location."&key=".$googleAPI;

//recebe um resposta da api
$json = file_get_contents($url,0,null,null);
$json_output = json_decode($json, true);
//navega pela estrutura de dados para ir buscar a cover
$lat = $json_output['results'][0]['geometry']['location']['lat'];
$lng = $json_output['results'][0]['geometry']['location']['lng'];

//imprimir informação
echo $lat.",".$lng;

//registar pedido
include './pedidos/registarPedido.php';
echo getUrlPedido("/getCoordenadas?location=".$location);
?> 

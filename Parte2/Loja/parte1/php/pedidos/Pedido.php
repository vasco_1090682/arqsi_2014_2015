<?php
class Pedido{
	public $id;
	public $pedido;
	public $data;
	public $hora;
	public $browser;
	
	function __construct($id, $pedido, $data, $hora, $browser) {
		$this->id = $id;
		$this->pedido = $pedido;
		$this->data = $data;
		$this->hora = $hora;
		$this->browser = $browser;
	}
}
?>
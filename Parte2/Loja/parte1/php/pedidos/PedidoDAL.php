<?php
class PedidoDAL {
    //campos de dados para o objecto connection
    private $conn;
    private $table = 'Pedidos';
    
    function __construct(){
        require("credenciaisAutenticacao.php");
        $this->conn = mysql_connect($db_server, $db_username, $db_password);
        if (!($this->conn)) {
            echo "Não foi possivel ligar à base de dados: " . mysql_error();
            exit();
        }
		
        if(!mysql_select_db($db_name)){
            echo "Não foi possivel seleccionar a base de dados" . mysql_error();
            exit();
        }
    }
    
    
    function __destruct() {
        mysql_close($this->conn);
    }
    
    public function query($sql) {   //instruções SELECT
        $result = mysql_query($sql);
        return $this->array_objectos($result);
    }
    
    public function nonQuery($sql) { //instruções INSERT, UPDATE, DELETE
        mysql_query($sql);
        return mysql_affected_rows();
    }
    
    
    public function array_objectos($result){
        while ($row = mysql_fetch_array($result))
                $pedidos[] = new Pedido(
                                        $row["id"],
                                        $row["pedido"],
                                        $row["data"],
                                        $row["hora"],
                                        $row["browser"]);
        return $pedidos;
    }
}
?>
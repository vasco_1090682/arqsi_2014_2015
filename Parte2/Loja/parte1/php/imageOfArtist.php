<?
require_once ("apis_keys.php");
//rawurlencode para poder enviar parametros com caracteres especiais
$artista=rawurlencode($_GET['artista']); //nome do artista
//7 = small
//9 = medium
//11 = large
//13 = extralarge
//15 = mega
$size=rawurlencode($_GET['size']);

//validar size da imagem
if ($size == 7 || $size == 9 || $size == 11 || $size == 13 || $size == 15)
{
	// pedido ao last.fm com a função file_gets_contents
	// a string XML devolvida pelo servidor last.fm fica armazenada na variável $respostaXML
	$respostaXML=file_get_contents("http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist=".$artista."&api_key=".$lastfmAPI);

	// criar um objecto DOMDocument e inicializá-lo com a string XML recebida
	$newXML= new DOMDocument('1.0', 'ISO-8859-1');
	$newXML->loadXML($respostaXML);

	// navegar no XML com os métodos que já conhece, mas com uma sintaxe PHP para
	// aceder a objectos(->)
	$nodelist=$newXML->getElementsByTagName("lfm");
	$tagNode=$nodelist->item($i)->childNodes->item(1)->childNodes->item($size);
	$tags=$tagNode->nodeValue;

	//imprimir informação
	echo $tags;
} else {
	echo "Tamanho da imagem inválido";
}

//registar pedido
include './pedidos/registarPedido.php';
echo getUrlPedido("/imageOfArtist?artista=".$artista."&size=".$size);
?> 

<?
require_once ("apis_keys.php");
//rawurlencode para poder enviar parametros com caracteres especiais
$artista=rawurlencode($_GET['artista']); //nome do artista

// pedido ao last.fm com a função file_gets_contents
// a string XML devolvida pelo servidor last.fm fica armazenada na variável $respostaXML
$respostaXML=file_get_contents("http://ws.audioscrobbler.com/2.0/?method=artist.gettoptags&artist=".$artista."&api_key=".$lastfmAPI);

// criar um objecto DOMDocument e inicializá-lo com a string XML recebida
$newXML= new DOMDocument('1.0', 'ISO-8859-1');
$newXML->loadXML($respostaXML);

// navegar no XML com os métodos que já conhece, mas com uma sintaxe PHP para
// aceder a objectos(->)
$nodelist=$newXML->getElementsByTagName("name");
$tags="";
for ($i=0;$i<$nodelist->length;$i++)
{
  $tagNode=$nodelist->item($i);
  $tagValue = $tagNode->nodeValue;
  
  //não colocar vírgula após a última tag
  if ($i+1 == $nodelist->length)
    $tags.=$tagValue;
  else
    $tags.=$tagValue . ",";
  
}
//imprimir informação
echo $tags;

//registar pedido
include './pedidos/registarPedido.php';
echo getUrlPedido("/topTagsByArtist?artista=".$artista);
?> 

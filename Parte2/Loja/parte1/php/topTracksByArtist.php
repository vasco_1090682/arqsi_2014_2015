<?
require_once ("apis_keys.php");
//rawurlencode para poder enviar parametros com caracteres especiais
$artista=rawurlencode($_GET['artista']); //nome do artista
$ntracks=rawurlencode($_GET['ntracks']); //número de tracks a mostrar

// pedido ao last.fm com a função file_gets_contents
// a string XML devolvida pelo servidor last.fm fica armazenada na variável $respostaXML
$respostaXML=file_get_contents("http://ws.audioscrobbler.com/2.0/?method=artist.gettoptracks&artist=".$artista."&limit=".$ntracks."&api_key=".$lastfmAPI);

// criar um objecto DOMDocument e inicializá-lo com a string XML recebida
$newXML= new DOMDocument('1.0', 'ISO-8859-1');
$newXML->loadXML($respostaXML);

// navegar no XML com os métodos que já conhece, mas com uma sintaxe PHP para
// aceder a objectos(->)
$nodelist=$newXML->getElementsByTagName("track");
$tracks="";
for ($i=0;$i<$nodelist->length;$i++)
{
  $trackNameNode=$nodelist->item($i)->childNodes->item(1); //primeiro filho de track que é o name da track
  $trackName = $trackNameNode->nodeValue;
  
  //não colocar vírgula após a última track
  if ($i+1 == $nodelist->length)
    $tracks.=$trackName;
  else
    $tracks.=$trackName . ",";
  
}

//imprimir informação
echo $tracks;

//registar pedido
include './pedidos/registarPedido.php';
echo getUrlPedido("/topTracksByArtist?artista=".$artista."&ntracks=".$ntracks);
?> 

<?
require_once ("apis_keys.php");
//rawurlencode para poder enviar parametros com caracteres especiais
$mbid=rawurlencode($_GET['mbid']); //mbid da track

// pedido ao last.fm com a função file_gets_contents
// a string XML devolvida pelo servidor last.fm fica armazenada na variável $respostaXML
$respostaXML=file_get_contents("http://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key=".$lastfmAPI."&mbid=".$mbid);

// criar um objecto DOMDocument e inicializá-lo com a string XML recebida
$newXML= new DOMDocument('1.0', 'ISO-8859-1');
$newXML->loadXML($respostaXML);

// navegar no XML com os métodos que já conhece, mas com uma sintaxe PHP para
// aceder a objectos(->)
$nodelist=$newXML->getElementsByTagName("album");
$albumTitle=$nodelist->item(0)->childNodes->item(3)->nodeValue; //title do album
$mbidAlbum = $nodelist->item(0)->childNodes->item(5)->nodeValue; //mbid do album
$albumInfo = trim($albumTitle).",".trim($mbidAlbum);

//imprimir informação
echo $albumInfo;

//registar pedido
include './pedidos/registarPedido.php';
echo getUrlPedido("/albumOfTrack?mbid=".$mbid);
?> 

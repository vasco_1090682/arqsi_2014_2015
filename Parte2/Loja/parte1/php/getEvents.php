<?
require_once ("apis_keys.php");
//rawurlencode para poder enviar parametros com caracteres especiais
$location=rawurlencode($_GET['location']); //localização indicada
$distance=rawurlencode($_GET['distance']); //distância indicada
$limit=100; // número máximo de eventos a pedir à api

// pedido ao last.fm com a função file_gets_contents
// a string XML devolvida pelo servidor last.fm fica armazenada na variável $respostaXML
$respostaXML=file_get_contents("http://ws.audioscrobbler.com/2.0/?method=geo.getevents&location=".$location."&distance=".$distance."&limit=".$limit."&api_key=".$lastfmAPI);

// criar um objecto DOMDocument e inicializá-lo com a string XML recebida
$newXML= new DOMDocument('1.0', 'ISO-8859-1');
$newXML->loadXML($respostaXML);

// navegar no XML com os métodos que já conhece, mas com uma sintaxe PHP para
// aceder a objectos(->)
$nodelist=$newXML->getElementsByTagName("event");

//construir xml
$xml = '<?xml version="1.0" encoding="UTF-8"?>';
$xml .= "\n<events>\n";

//tag para as coordenadas da localização indicada
$coordenadas = file_get_contents("http://phpdev2.dei.isep.ipp.pt/~i090682/trabalho/php/getCoordenadas?location=".$location);
$xml .= "<coordenadas>".trim($coordenadas)."</coordenadas>";

for ($i=0;$i<$nodelist->length;$i++)
{
  $xml .= "<event>\n";

  //nome do evento  
  $eventNameNode=$nodelist->item($i)->getElementsByTagName("title");
  $eventName=$eventNameNode->item(0)->childNodes->item(0)->nodeValue;
  $xml .= "<name>".trim(xml_entities($eventName))."</name>\n";
  
  //data do evento
  $eventDateNode=$nodelist->item($i)->getElementsByTagName("startDate");
  $eventDate=$eventDateNode->item(0)->childNodes->item(0)->nodeValue;
  $xml .= "<date>".trim(xml_entities($eventDate))."</date>\n";
  
  //latitude
  $eventLatNode=$nodelist->item($i)->getElementsByTagName("lat");
  $eventLat=$eventLatNode->item(0)->childNodes->item(0)->nodeValue;
  $xml .= "<latitude>".trim(xml_entities($eventLat))."</latitude>\n";
  
  //longitude
  $eventLongNode=$nodelist->item($i)->getElementsByTagName("long");
  $eventLong=$eventLongNode->item(0)->childNodes->item(0)->nodeValue;
  $xml .= "<longitude>".trim(xml_entities($eventLong))."</longitude>\n";

  $xml .= "</event>\n";
}
$xml .= "</events>";

Header('Content-type: text/xml');
echo $xml;

//métodos auxiliares
//substituir caracteres especiais
function xml_entities($string) {
    return str_replace(
        array("&",     "<",    ">",    '"',      "'"),
        array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"), 
        $string
    );
}

//registar pedido
include './pedidos/registarPedido.php';
echo getUrlPedido("/getEvents?location=".$location."&distance=".$distance);
?> 
